kotlin {
    explicitApi()
}

dependencies {
    implementation(kotlin("stdlib"))
    implementation(kotlin("reflect"))

    implementation("org.jetbrains.kotlinx:kotlinx-serialization-json:1.3.3")
    implementation("io.github.microutils:kotlin-logging:2.1.23")
    implementation("io.arrow-kt:arrow-core:1.1.2")
    implementation("com.squareup.okio:okio:3.2.0")
}
