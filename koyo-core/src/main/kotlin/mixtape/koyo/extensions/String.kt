package mixtape.koyo.extensions

public fun String.substringAround(start: String, end: String): String? {
    val start = substringAfter(start, "").takeUnless { it.isEmpty() }
        ?: return null

    return start.substringBefore(end, "").takeUnless { it.isEmpty() }
}

public fun String.truncate(size: Int, includeDots: Boolean = true): String {
    val suffix = if (includeDots && length > size) "..." else ""
    return take(size) + suffix
}

public fun String.capitalize(): String = replaceFirstChar { it.uppercase() }

