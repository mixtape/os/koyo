package mixtape.koyo.extensions

import arrow.core.*

public fun <T> Option<T>.expect(message: String): T = 
    getOrElse { throw NullPointerException(message) }

public fun <T> Option<T>.unwrap(): T =
    expect("this to be Some")
