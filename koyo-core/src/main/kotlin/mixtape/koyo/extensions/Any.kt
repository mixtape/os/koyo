package mixtape.koyo.extensions

import kotlin.contracts.contract

public inline fun <reified T> Any?.intoOrNull(): T? =
    this as? T

public inline fun <reified T> Any?.into(): T {
    contract {
        returns() implies (this@into is T)
    }

    return requireNotNull(intoOrNull<T>()) { "Cannot cast ${this?.javaClass?.canonicalName} to ${T::class.java.canonicalName}" }
}
