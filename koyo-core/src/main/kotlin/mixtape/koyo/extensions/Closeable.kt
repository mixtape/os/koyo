package mixtape.koyo.extensions

public inline fun <T : AutoCloseable?, R> use(closeable: T, block: (T) -> R): R =
    closeable.use(block)
