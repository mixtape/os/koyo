package mixtape.koyo

import java.util.UUID

/**
 * Generates a unique id.
 * Uses [UUID]
 *
 * @return A unique id
 */
public fun generateUniqueId(): String =
    UUID.randomUUID().toString().filterNot { it in "-" }
