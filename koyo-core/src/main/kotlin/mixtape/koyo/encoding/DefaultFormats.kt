package mixtape.koyo.encoding

import kotlinx.serialization.json.Json

/**
 * Default formats used throughout this monorepo.
 */
public object DefaultFormats {
    public val JSON: Json = Json {
        isLenient = true
        encodeDefaults = true
        ignoreUnknownKeys = true
    }
}
