package mixtape.koyo.encoding.json.annotation

import kotlinx.serialization.SerialInfo

@Target(AnnotationTarget.CLASS)
@SerialInfo
public annotation class UsedAsUnknown
