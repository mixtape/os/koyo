package mixtape.koyo.encoding.json

import kotlinx.serialization.KSerializer
import kotlinx.serialization.json.JsonElement
import kotlinx.serialization.json.JsonObject

public fun Map<String, JsonElement>.toJsonObject(): JsonObject =
    JsonObject(this)

public fun <T> KSerializer<T>.wrapped(fieldName: String? = null): KSerializer<T> {
    if (fieldName == null) {
        return WrappedSerializer(this)
    }

    return WrappedSerializer(this, fieldName)
}
