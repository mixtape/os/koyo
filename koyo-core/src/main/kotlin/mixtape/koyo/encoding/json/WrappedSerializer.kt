package mixtape.koyo.encoding.json

import arrow.core.Nel
import arrow.core.nel
import kotlinx.serialization.KSerializer
import kotlinx.serialization.SerializationException
import kotlinx.serialization.descriptors.SerialDescriptor
import kotlinx.serialization.descriptors.buildClassSerialDescriptor
import kotlinx.serialization.encoding.Decoder
import kotlinx.serialization.encoding.Encoder
import kotlinx.serialization.json.JsonDecoder
import kotlinx.serialization.json.JsonEncoder
import kotlinx.serialization.json.JsonObject
import kotlinx.serialization.json.jsonObject
import mixtape.koyo.encoding.json.annotation.JsonWrapped
import mixtape.koyo.extensions.into
import mixtape.koyo.extensions.intoOrNull

public open class WrappedSerializer<T>(
    public val parent: KSerializer<T>,
    public val fieldName: String,
) : KSerializer<T> {
    public companion object {
        internal fun <T> getFieldNamesFor(serializer: KSerializer<T>): Nel<String> {
            val descriptor = serializer.descriptor

            /* attempt to find field name from the @JsonWrapped annotation */
            val anno = descriptor.annotations.firstNotNullOfOrNull { it.intoOrNull<JsonWrapped>() }
            if (anno != null) {
                return Nel.fromListUnsafe(anno.fieldNames.toList())
            }

            /* extract the final name from the descriptor serial name, e.g. com.example.TabRenderer -> tabRenderer */
            return descriptor.serialName
                .substringAfterLast('.')
                .replaceFirstChar { it.lowercase() }
                .nel()
        }
    }

    public constructor(serializer: KSerializer<T>) : this(serializer, getFieldNamesFor(serializer).first())

    override val descriptor: SerialDescriptor = buildClassSerialDescriptor("WrappedClass[$fieldName]") {
        element(fieldName, parent.descriptor)
    }

    override fun serialize(encoder: Encoder, value: T) {
        val output = encoder.into<JsonEncoder>()

        val element = output.json.encodeToJsonElement(parent, value)

        val json = JsonObject(mapOf(fieldName to element))

        encoder.encodeSerializableValue(JsonObject.serializer(), json)
    }

    override fun deserialize(decoder: Decoder): T {
        val input = decoder.into<JsonDecoder>()

        val element = input.decodeJsonElement().jsonObject[fieldName]?.jsonObject
            ?: throw SerializationException("Expected JSON object with field $fieldName")

        return input.json.decodeFromJsonElement(parent, element)
    }
}
