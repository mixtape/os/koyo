package mixtape.koyo.encoding.json

import kotlinx.serialization.DeserializationStrategy
import kotlinx.serialization.json.Json
import kotlinx.serialization.serializer
import mixtape.koyo.encoding.DefaultFormats

public inline fun <reified T : Any> String.deserialize(json: Json = DefaultFormats.JSON): T =
    json.decodeFromString(T::class.serializer(), this)

public fun <T> String.deserialize(deserializer: DeserializationStrategy<T>, json: Json = DefaultFormats.JSON): T =
    json.decodeFromString(deserializer, this)
