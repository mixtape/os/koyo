package mixtape.koyo.encoding.json

import kotlinx.serialization.KSerializer
import kotlinx.serialization.SerializationException
import kotlinx.serialization.descriptors.PolymorphicKind
import kotlinx.serialization.descriptors.SerialDescriptor
import kotlinx.serialization.descriptors.buildSerialDescriptor
import kotlinx.serialization.encoding.Decoder
import kotlinx.serialization.encoding.Encoder
import kotlinx.serialization.json.JsonDecoder
import kotlinx.serialization.json.JsonObject
import kotlinx.serialization.serializer
import kotlinx.serialization.serializerOrNull
import mixtape.koyo.encoding.json.annotation.UsedAsUnknown
import mixtape.koyo.extensions.into
import kotlin.reflect.KClass

public open class PolymorphicWrappedSerializer<T : Any>(
    public val baseClass: KClass<T>,
    public val createUnknown: ((JsonObject) -> T)? = null,
) : KSerializer<T> {
    private val children = baseClass.sealedSubclasses.map { kc ->
        val serializer = kc.serializer()
        val fieldNames = WrappedSerializer.getFieldNamesFor(serializer)
        PossibleWrapper(kc, serializer, fieldNames, serializer.descriptor.annotations.any { it is UsedAsUnknown })
    }

    override val descriptor: SerialDescriptor =
        buildSerialDescriptor(baseClass.qualifiedName + "[Polymorphic+Wrapped]", PolymorphicKind.SEALED)

    init {
        require(children.filter { it.isBackup }.size <= 1) {
            "Only one class can be marked as backup"
        }
    }

    override fun serialize(encoder: Encoder, value: T) {
        val actualSerializer = (encoder.serializersModule.getPolymorphic(baseClass, value)
            ?: value::class.serializerOrNull()
            ?: error("well fuck"))
            .into<KSerializer<T>>()

        return encoder.encodeSerializableValue(actualSerializer.wrapped(), value)
    }

    override fun deserialize(decoder: Decoder): T {
        val input = decoder.into<JsonDecoder>()
        val element = input.decodeJsonElement()

        if (element !is JsonObject) {
            throw SerializationException("Expected JsonObject, got $element")
        }

        val wrapper: Pair<String, PossibleWrapper> = element.keys
            .firstNotNullOfOrNull { key -> children.firstOrNull { it.fieldNames.contains(key) }?.let { key to it } }
            ?: return createUnknown?.invoke(element) ?: throw SerializationException("[WrappedSerializer] unknown type(s): ${element.keys}")

        return input.json.decodeFromJsonElement(
            wrapper.second.serializer.wrapped(wrapper.first),
            element
        )
    }

    public inner class PossibleWrapper(
        public val kClass: KClass<out T>,
        public val serializer: KSerializer<out T>,
        public val fieldNames: List<String>,
        public val isBackup: Boolean,
    )
}
