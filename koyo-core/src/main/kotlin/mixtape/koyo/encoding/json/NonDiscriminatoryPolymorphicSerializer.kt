package mixtape.koyo.encoding.json

import kotlinx.serialization.KSerializer
import kotlinx.serialization.descriptors.SerialDescriptor
import kotlinx.serialization.encoding.Decoder
import kotlinx.serialization.encoding.Encoder
import kotlinx.serialization.json.JsonClassDiscriminator
import kotlinx.serialization.json.JsonEncoder
import kotlinx.serialization.json.JsonObject
import mixtape.koyo.extensions.into

public open class NonDiscriminatoryPolymorphicSerializer<T>(public val base: KSerializer<T>) : KSerializer<T> {
    override val descriptor: SerialDescriptor
        get() = base.descriptor

    override fun serialize(encoder: Encoder, value: T) {
        with (encoder.into<JsonEncoder>()) {
            val jsonElement = json.encodeToJsonElement(base, value)
            if (jsonElement is JsonObject) {
                val anno = base.descriptor.annotations
                    .find { it is JsonClassDiscriminator }
                    ?.into<JsonClassDiscriminator>()

                encodeJsonElement(jsonElement
                    .filterKeys { it != (anno?.discriminator ?: "type") }
                    .toJsonObject())
            } else {
                encodeJsonElement(jsonElement)
            }
        }
    }

    override fun deserialize(decoder: Decoder): T =
        base.deserialize(decoder)
}
