package mixtape.koyo.encoding.base64

import okio.ByteString
import okio.ByteString.Companion.toByteString

/**
 * Encodes this string into base64.
 */
public fun ByteArray.encodeBase64String(): String =
    toByteString().base64()

/**
 * Encodes this string into base64.
 */
public fun String.encodeBase64String(): String =
    with (ByteString) { encode().base64() }

/**
 * Encodes this string into base64.
 */
public fun String.decodeBase64Bytes(): ByteArray =
    with(ByteString) { decodeBase64()?.toByteArray()!! }

/**
 * Encodes this string into base64.
 */
public fun String.decodeBase64String(): String =
    with(ByteString) { decodeBase64()!!.string(Charsets.UTF_8) }
