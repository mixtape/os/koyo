package mixtape.koyo.lang.java

import kotlin.properties.ReadOnlyProperty
import kotlin.reflect.KProperty

public class ThreadLocalDelegate<T>(public val provider: () -> T) : ReadOnlyProperty<Any?, T> {
    private val threadLocal = ThreadLocal.withInitial { provider() }

    override fun getValue(thisRef: Any?, property: KProperty<*>): T =
        threadLocal.get()
}
