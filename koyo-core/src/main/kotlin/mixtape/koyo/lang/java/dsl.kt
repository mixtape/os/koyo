package mixtape.koyo.lang.java

import kotlin.properties.ReadOnlyProperty

public fun <T> threadLocal(supplier: () -> T): ReadOnlyProperty<Any?, T> =
    ThreadLocalDelegate(supplier)
