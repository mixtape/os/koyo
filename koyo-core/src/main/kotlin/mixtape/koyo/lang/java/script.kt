package mixtape.koyo.lang.java

import javax.script.ScriptEngine
import javax.script.ScriptEngineManager

/**
 * Returns a new script engine with the given [name]
 *
 * @param name The name of the script engine
 */
public fun scriptEngineFor(name: String): ScriptEngine? = ScriptEngineManager().getEngineByName(name)
