package mixtape.koyo.io.net.useragent

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable
import kotlinx.serialization.json.decodeFromStream
import mixtape.koyo.extensions.use
import mixtape.koyo.log.logging
import mixtape.koyo.encoding.DefaultFormats
import mixtape.koyo.io.net.NetTools
import java.util.zip.GZIPInputStream

@Serializable
public data class UserAgent(
    val appName: String,
    val connection: Connection? = null,
    val platform: String,
    val pluginsLength: Long = 0,
    val vendor: String? = null,
    @SerialName("userAgent")
    val asString: String,
    val viewportHeight: Int?,
    val viewportWidth: Int?,
    val deviceCategory: String,
    val screenHeight: Int,
    val screenWidth: Int,
    val weight: Double,
) {
    public companion object {
        public const val GZIP_URL: String = "https://github.com/intoli/user-agents/raw/master/src/user-agents.json.gz"

        public lateinit var ALL: List<UserAgent>
            private set

        private val log by logging { }

        /**
         * Retrieve a random user agent.
         * @see loadUserAgents
         */
        public fun retrieveRandom(): UserAgent {
            ensureLoaded()
            return ALL.random()
        }

        /**
         * Retrieve a user agent of the supplied [deviceCategory] or a random one if none could be found.
         * @see loadUserAgents
         * @see ofCategory
         * @see retrieveRandom
         */
        public fun ofPlatformOrRandom(deviceCategory: String = "desktop"): UserAgent =
             ofCategory(deviceCategory) ?: retrieveRandom()


        /**
         * Retrieve a user agent of the supplied [platform].
         * @see loadUserAgents
         */
        public fun ofCategory(deviceCategory: String = "desktop"): UserAgent? {
            ensureLoaded()
            return ALL.filter { it.deviceCategory == deviceCategory }.randomOrNull()
        }

        private fun ensureLoaded() {
            /* make sure the list of user agents has been loaded */
            if (!Companion::ALL.isInitialized) loadUserAgents()
        }

        /**
         * Loads the list of random user agents.
         */
        public fun loadUserAgents() {
            ALL = use(GZIPInputStream(NetTools.downloadStream(GZIP_URL))) { gz ->
                DefaultFormats.JSON.decodeFromStream(gz)
            }

            log.info { "Loaded ${ALL.size} user agents." }
        }
    }

    @Serializable
    public data class Connection(
        val downlink: Float = 1f,
        val effectiveType: String = "",
        val rtt: Int = 1
    )
}
