package mixtape.koyo.io.net

import mixtape.koyo.extensions.into
import java.io.InputStream
import java.net.HttpURLConnection
import java.net.URL

public object NetTools {
    public fun download(url: String): String {
        return downloadStream(url)
            .reader()
            .readText()
    }

    public fun downloadStream(url: String): InputStream {
        val connection = URL(url)
            .openConnection()
            .into<HttpURLConnection>()

        connection.connect()
        connection.disconnect()
        require(connection.responseCode == 200) {
            "Could not fetch $url"
        }

        return connection.inputStream
    }
}
