package mixtape.koyo.log

import mu.KLogger
import mu.KotlinLogging
import kotlin.properties.ReadOnlyProperty

/**
 *
 */
public fun logging(func: () -> Unit): ReadOnlyProperty<Any?, KLogger> = ReadOnlyProperty { _, _ ->
    KotlinLogging.logger(func)
}

/**
 *
 */
public fun logging(name: String): ReadOnlyProperty<Any?, KLogger> = ReadOnlyProperty { _, _ ->
    KotlinLogging.logger(name)
}
