rootProject.name = "Koyo"

include(":koyo-core")
include(":koyo-js")

enableFeaturePreview("TYPESAFE_PROJECT_ACCESSORS")
