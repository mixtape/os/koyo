package mixtape.koyo.lang.js

/**
 * Object for using javascript JSON via GraalJS
 */
public object JSON {
    /**
     * Calls `JSON.stringify(source)` in a JavaScript context.
     *
     * @param source The source to stringify.
     */
    public fun stringify(source: String): String? {
        return evaluateJs("JSON.stringify($source);")?.asString()
    }
}
