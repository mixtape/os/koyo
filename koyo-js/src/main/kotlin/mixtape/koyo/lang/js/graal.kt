package mixtape.koyo.lang.js

import mixtape.koyo.lang.java.threadLocal
import org.graalvm.polyglot.Context
import org.graalvm.polyglot.Value

/**
 * A thread-local javascript context using GraalJS.
 */
public val jsContext: Context by threadLocal { createJsContext() }

/**
 * Creates a new [Context] for JavaScript execution.
 */
public fun createJsContext(block: Context.Builder.() -> Unit = {}): Context {
    System.setProperty("polyglot.engine.WarnInterpreterOnly", "false")
    return Context.newBuilder()
        .apply(block)
        .build()
}

/**
 * Evaluates the given JavaScript code in the supplied [context].
 *
 * @param script  the JavaScript code to evaluate
 * @param context the context to evaluate the code with
 * @param scope   the scope to evaluate the code in.
 * @return the result of the evaluation
 */
public fun evaluateJs(
    script: String,
    context: Context = jsContext,
): Value? {
    synchronized(context) {
        return context.eval("js", script)
    }
}
