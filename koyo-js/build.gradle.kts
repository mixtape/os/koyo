kotlin {
    explicitApi()
}

dependencies {
    implementation(projects.koyoCore)

    implementation("org.graalvm.js:js:22.1.0.1")
}
