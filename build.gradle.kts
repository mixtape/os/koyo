import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

plugins {
    kotlin("jvm") version "1.7.0" apply false
    kotlin("plugin.serialization") version "1.7.0" apply false
}

subprojects {
    apply(plugin = "kotlin")
    apply(plugin = "kotlinx-serialization")
    apply(plugin = "maven-publish")

    val project = this
    repositories {
        mavenCentral()
    }


    val sourceSets = extensions.getByName("sourceSets") as SourceSetContainer
    tasks {
        val sourcesJar by registering(Jar::class) {
            archiveClassifier.set("sources")
            from(sourceSets.named("main").get().allSource)
        }

        extensions.configure<PublishingExtension> {
            repositories {
                maven("https://maven.dimensional.fun/releases") {
                    authentication {
                        create<BasicAuthentication>("basic")
                    }

                    credentials {
                        username = getConfigurationValue("REPO_ALIAS")
                        password = getConfigurationValue("REPO_TOKEN")
                    }
                }
            }

            publications {
                val projectNameNormalized = project.name.capitalize()
                    .split('-')
                    .joinToString("") { it.capitalize() }

                create<MavenPublication>(projectNameNormalized) {
                    from(project.components["java"])

                    group = "mixtape.oss.koyo"
                    artifactId = project.name
                    version = "0.2.1"

                    artifact(sourcesJar)

                    pom {
                        name.set(project.name)
                        description.set("A collection of utility libraries for solving common problems.")
                        url.set("https://mixtape.systems/")

                        organization {
                            name.set("Dimensional Fun")
                            url.set("https://www.dimensional.fun/")
                        }

                        developers {
                            developer { name.set("Mixtape Bot") }
                        }

                        issueManagement {
                            system.set("GitLab")
                            url.set("https://gitlab.com/mixtape/os/koyo/-/issues")
                        }

                        scm {
                            connection.set("scm:git:ssh://gitlab.com/mixtape/os/koyo.git")
                            developerConnection.set("scm:git:ssh://git@gitlab.com:mixtape/os/koyo.git")
                            url.set("https://mixtape.systems/")
                        }
                    }
                }
            }
        }

        withType<KotlinCompile> {
            kotlinOptions {
                freeCompilerArgs = listOf(
                    "-opt-in=kotlin.RequiresOptIn",
                    "-opt-in=kotlin.contracts.ExperimentalContracts",
                    "-opt-in=kotlinx.serialization.ExperimentalSerializationApi",
                    "-opt-in=kotlinx.serialization.InternalSerializationApi",
                )
            }
        }
    }
}

fun getConfigurationValue(name: String): String? {
    return System.getenv(name)
        ?: System.getProperty(name.toLowerCase().replace('_', '.'))
}

